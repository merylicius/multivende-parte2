import axios from "axios";

const baseUrl = "https://app.multivende.com/oauth/access-token";
const clientId = 896123781342;

export const consult = async (codeparam) => {
  try {
    const response = await axios({
      url: `${baseUrl}`,
      method: "POST",
      data: {
        client_id: clientId,
        client_secret: process.env.REACT_APP_API_KEY,
        grant_type: "authorization_code",
        code: `${codeparam}`,
      },
    });
    localStorage.setItem("token", JSON.stringify(response.data.token));

    localStorage.setItem("merchantId",JSON.stringify(response.data.MerchantId));

    localStorage.setItem("refreshToken",JSON.stringify(response.data.refreshToken));


    console.log(response.data.token);
    return response.data.token;
  } catch (error) {
    console.log(error);
  }
};

const tokencode = JSON.parse(localStorage.getItem("token"));
console.log("", tokencode);

export const createProviders = async (Id, code) => {
  try {
    const response = await axios({
      url: `https://app.multivende.com/api/m/${Id}/providers`,
      method: "POST",
      headers: {
        Authorization: `Bearer ${code}`,
      },
    });

    console.log(response);
    return response;
  } catch (error) {
    console.log(error);
  }
};

export const editProviders = async (
  provider_id,
  name,
  rut,
  giro,
  email,
  phoneNumber,
  country,
  address,
  zipCode,
  description
) => {
  console.log("email", email);
  try {
    const response = await axios({
      url: `https://app.multivende.com/api/providers/${provider_id}`,
      method: "PUT",
      headers: {
        Authorization: `Bearer ${tokencode}`,
      },
      data: {
        name: name,
        code: rut,
        giro: giro,
        email: email,
        phoneNumber: phoneNumber,
        country: country,
        address: address,
        zipCode: zipCode,
        description: description,
      },
    });

    console.log(response);
    return response;
  } catch (error) {
    console.log(error);
  }
};

export const deleteProviders = async (id) => {
  try {
    const response = await axios({
      url: `https://app.multivende.com/api/providers/${id}`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${tokencode}`,
      },
    });

    return response;
  } catch (error) {
    console.log(error);
  }
};

export const consultProviders = async () => {
const merchanId = JSON.parse(localStorage.getItem("merchantId"));
const tokenode = JSON.parse(localStorage.getItem("token"));
  console.log('hola', merchanId )

  try {
    const response = await axios({
      url: `https://app.multivende.com/api/m/${merchanId}/providers/p/1`,
      headers: {
        Authorization: `Bearer ${tokenode}`,
      },
    });

    return await response.data.entries;
  } catch (error) {
    console.log(error);
  }
};

export const islogged = ()=>{
  const token = JSON.parse(localStorage.getItem("token"));
  console.log(token)
  if(token){
    return true;
  }else return false;
}

export const consultProvidersId = async (providerId) => {
  try {
    const response = await axios({
      url: `https://app.multivende.com/api/providers/${providerId}`,
      headers: {
        Authorization: `Bearer ${tokencode}`,
      },
    });

    console.log("editarid", response.data);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const consultWhareHouse = async () => {
  const merchanId = JSON.parse(localStorage.getItem("merchantId"));
  const tokenode = JSON.parse(localStorage.getItem("token"));
  
    try {
      const response = await axios({
        url: `https://app.multivende.com/api/m/${merchanId}/stores-and-warehouses/p/1`,
        headers: {
          Authorization: `Bearer ${tokenode}`,
        },
      });
  
      return await response.data.entries;
    } catch (error) {
      console.log(error);
    }
  };

  export const consultStocks = async (whareHouseId) => {
    const merchanId = JSON.parse(localStorage.getItem("merchantId"));
    const tokenode = JSON.parse(localStorage.getItem("token"));
    console.log('wharehouse',whareHouseId)
      try {
        const response = await axios({
          url: `https://app.multivende.com/api/m/${merchanId}/product-versions/p/1?_include_stock=true&_warehouse_id=${whareHouseId}`,
          headers: {
            Authorization: `Bearer ${tokenode}`,
          },
        });
    console.log(response.data.entries)
        return await response.data.entries;
      } catch (error) {
        console.log(error);
      }
    };


