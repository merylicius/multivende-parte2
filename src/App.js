import { AppRouter } from "./routes/AppRoute";


function App() {

  
  return (
    <div className="container">
    <AppRouter/>
    </div>
  );
}

export default App;
