import React from 'react'
import { NavLink } from 'react-router-dom'
import "../../css/global.css";

export const NavBar = () => {
    return (
        <>

            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container-fluid">
                    <a className="navbar-brand" > <img className="nav-img" alt="" src="./assets/img/logo.png"></img></a>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <NavLink
                                activeClassName="active"
                                className="nav-item nav-link"
                                exact
                                to="/"
                            >
                                Inicio
                            </NavLink>
                            <NavLink 
                    activeClassName="active"
                    className="nav-item nav-link" 
                    exact
                    to="/provider"
                >
                    Proveedores
                </NavLink>

                <NavLink 
                    activeClassName="active"
                    className="nav-item nav-link" 
                    exact
                    to="/stock"
                >
                    Stocks
                </NavLink>
                        </ul>
                    </div>
                </div>
                
            </nav>

        </>
    )
}