import React, { useEffect, useState } from "react";
import "../../css/global.css";

import { Link } from "react-router-dom";
import { consultStocks, consultWhareHouse } from "../../services/Service";

export const Stock = () => {
  const [WhareH, setWhareH] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [whareHouse, setWhareHouse] = useState(false);
  const [stock, setstock] = useState([]);
  // const [amount, setAmount] = useState('')

  useEffect(() => {
    const consultWharehouseTable = async () => {
      try {
        const response = await consultWhareHouse();
        setSpinner(true);
        setWhareH(response);
      } catch (error) {
        console.log(error);
      }
    };
    setSpinner(false);
    consultWharehouseTable();
  }, []);

  const consultStocksWharewHouse = async (whareHouse) => {
    const stocks = await consultStocks(whareHouse);
    setstock(stocks);
  };
               

  const handleChange = (e) => {
    setWhareHouse(true);
    consultStocksWharewHouse(e.target.value);
  };

  return (
    <>
      <select
        onChange={handleChange}
        className="form-select form-select-sm mt-5 select"
        aria-label="Default select example"
      >
        <option selected disabled>
          Seleccione una Bodega
        </option>
        {WhareH.map((resp) => {
          return (
            <option key={resp._id} value={resp._id}>
              {resp.name}
            </option>
          );
        })}
      </select>
      {
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Nombre Producto</th>
              <th scope="col">Stock</th>
            </tr>
          </thead>
          <tbody>
            {whareHouse ? (
              stock.length > 0 ? (
                stock.map(({Product, ProductStocks}) => (
                  <tr key={Product._id}>
                    <td>{Product.name}</td>
                    <td>
                      <input value={ProductStocks.length>0? ProductStocks[0]['amount']: ''}></input>
                    </td>
                  </tr>
                ))
              ) : (

              <div class="spinner-border text-info" role="status">
                <span class="visually-hidden">Loading...</span>

                </div>
              )
            ) : null}
          </tbody>
        </table>
      }
    </>
  );
};
