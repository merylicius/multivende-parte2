import axios from "axios";
import React, { useState } from "react";
import { Redirect } from "react-router";
import { countries } from "../../data/countries";
import { useForm } from "../../hooks/useForm";

export const CreateProvider = () => {
  const [status, setStatus] = useState(false);
  const tokencode = JSON.parse(localStorage.getItem("token"));
  const merchantId = JSON.parse(localStorage.getItem("merchantId"));

  const [formValues, handleInputChange] = useForm({
    nombre: "",
    rut: "",
    giro: "",
    email: "",
    telefono: "",
    pais: "",
    direccion: "",
    code: "",
    descripcion: "",
  });

  const {
    nombre,
    rut,
    giro,
    email,
    telefono,
    pais,
    direccion,
    code,
    descripcion,
  } = formValues;

  const handleSubmit = (e) => {
    e.preventDefault();
    createProviders(merchantId, tokencode);
  };

  const createProviders = async (merchantId, token) => {
    try {
      const response = await axios({
        url: `https://app.multivende.com/api/m/${merchantId}/providers`,
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          name: nombre,
          email: email,
          taxId: rut,
          activity: giro,
          phoneNumber: telefono,
          description: descripcion,
          address: direccion,
          zipCode: code,
          locationId: pais,
        },
      });

      console.log(response.status);
      if (response.status === 201) {
        setStatus(true);
      }
      return response;
    } catch (error) {
      console.log(error);
    }
  };
  console.log(status);

  return (
    <div className="mt-5">
      {status ? <Redirect to="/provider" /> : null}
      <form onSubmit={handleSubmit}>
        <div class="row align-items-start">
          <div class="col">
            <label className="form-label">Nombre</label>
            <input
              type="text"
              className="form-control"
              name="nombre"
              value={nombre}
              onChange={handleInputChange}
            />
          </div>
          <div class="col">
            <label className="form-label">R.U.T</label>
            <input
              type="text"
              className="form-control"
              name="rut"
              value={rut}
              onChange={handleInputChange}
            />
          </div>
        </div>
        <div class="row align-items-start">
          <div class="col">
            <label className="form-label">Giro</label>
            <input
              type="text"
              className="form-control"
              name="giro"
              value={giro}
              onChange={handleInputChange}
            />
          </div>
          <div class="col">
            <label className="form-label">Email</label>
            <input
              type="email"
              className="form-control"
              name="email"
              value={email}
              onChange={handleInputChange}
            />
          </div>
        </div>
        <div class="row align-items-start">
          <div class="col">
            <label className="form-label">Número de Teléfono</label>
            <input
              type="number"
              className="form-control"
              name="telefono"
              value={telefono}
              onChange={handleInputChange}
            />
          </div>
          <div class="col">
            <label className="form-label">País</label>
            <select
              onChange={handleInputChange}
              className="form-select"
              aria-label="Default select example"
            >
              {countries.map((resp) => {
                return (
                  <option key={resp.id} value={resp.id}>
                    {resp.name}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
        <div className="mb-3"></div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">
            Dirección
          </label>
          <input
            type="text"
            className="form-control"
            name="direccion"
            value={direccion}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">
            Zip code
          </label>
          <input
            type="text"
            className="form-control"
            name="code"
            value={code}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">
            Descripción
          </label>
          <input
            type="text"
            className="form-control"
            name="descripcion"
            value={descripcion}
            onChange={handleInputChange}
          />
        </div>

        <button type="submit" className="btn btn-primary btn-sm margin-buttons width-button mb-3">
          Guardar
        </button>
        <button className="btn btn-warning btn-sm margin-buttons mb-3 width-button">Cancelar</button>
      </form>
    </div>
  );
};
