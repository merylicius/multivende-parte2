import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import { countries } from "../../data/countries";
import { useForm } from "../../hooks/useForm";
import { consultProvidersId, editProviders } from "../../services/Service";

export const EditProvider = () => {
  const { providerId } = useParams();

  const [provider, setProvider] = useState({});
  const [status, setStatus] = useState(false);

  useEffect(() => {
    const consultProviders = async () => {
      try {
        const response = await consultProvidersId(providerId);

        setProvider(response);
      } catch (error) {
        console.log(error);
      }
    };
    consultProviders();
  }, [providerId]);

  const {
    _id,
    name,
    rut,
    giro,
    email,
    phoneNumber,
    country,
    address,
    zipCode,
    description,
  } = provider;

  const [formValues, handleInputChange] = useForm({
    nombre: name,
    rut2: rut,
    giro2: giro,
    email2: email,
    telefono: phoneNumber,
    pais: country,
    direccion: address,
    code: zipCode,
    descripcion: description,
  });

  const {
    nombre,
    rut2,
    giro2,
    email2,
    telefono,
    pais,
    direccion,
    code,
    descripcion,
  } = formValues;
  const handleSubmit = async (e) => {
    e.preventDefault();

    const resp = await editProviders(
      _id,
      nombre,
      rut2,
      giro2,
      email2,
      telefono,
      pais,
      direccion,
      code,
      descripcion
    );
    console.log("responseedit", resp.status);

    if (resp.status === 200) {
      setStatus(true);
    }
  };

  return (
    <div className="mt-5 ">
      {status ? <Redirect to="/provider" /> : null}
      <form onSubmit={handleSubmit}>
        <div className="row align-items-start">
          <div className="col">
            <label className="form-label">Nombre</label>
            <input
              type="text"
              className="form-control"
              name="nombre"
              value={nombre || name || ""}
              onChange={handleInputChange}
            />
          </div>
          <div className="col">
            <label className="form-label">R.U.T</label>
            <input
              type="text"
              className="form-control"
              name="rut2"
              value={rut2 || rut || ""}
              onChange={handleInputChange}
            />
          </div>
        </div>
        <div className="row align-items-start">
          <div className="col">
            <label className="form-label">Giro</label>
            <input
              type="text"
              className="form-control"
              name="giro2"
              value={giro2 || giro || ""}
              onChange={handleInputChange}
            />
          </div>
          <div className="col">
            <label className="form-label">Email</label>
            <input
              type="email"
              className="form-control"
              name="email2"
              value={email2 || email || ""}
              onChange={handleInputChange}
            />
          </div>
        </div>
        <div className="row align-items-start">
          <div className="col">
            <label className="form-label">Número de Teléfono</label>
            <input
              type="number"
              className="form-control"
              name="telefono"
              value={telefono || phoneNumber || ""}
              onChange={handleInputChange}
            />
          </div>
          <div className="col">
            <label className="form-label">País</label>
            <select
              defaultValue={country}
              onChange={handleInputChange}
              className="form-select"
              aria-label="Default select example"
            >
              {countries.map((resp) => {
                return (
                  <option key={resp.id} value={resp.id}>
                    {resp.name}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
        <div className="mb-3"></div>
        <div className="mb-3">
          <label className="form-label">Dirección</label>
          <input
            type="text"
            className="form-control"
            name="direccion"
            value={direccion || address || ""}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Zip code</label>
          <input
            type="text"
            className="form-control"
            name="code"
            value={zipCode || code || ""}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Descripción</label>
          <input
            type="text"
            className="form-control"
            name="descripcion"
            value={descripcion || description || ""}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <button
            type="submit"
            className="btn btn-primary btn-sm margin-buttons width-button"
          >
            Guardar
          </button>
          <button className="btn btn-warning btn-sm margin-buttons width-button mb-10">
            Cancelar
          </button>
        </div>
      </form>
    </div>
  );
};
