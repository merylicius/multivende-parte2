import React from "react";

import Button from "@mui/material/Button";
import "../../css/global.css";


export const Login = ({ history }) => {


  const handleClick = () => {
    

    const url = 'https://app.multivende.com/apps/authorize?response_type=code&client_id=896123781342&redurect_uir=http://localhost:3000/configuration&scope=read:checkouts'
    window.location.assign(
      url
    );
  };

  return (
    <div className="login">
      <img className="login-img" alt="" src="./assets/img/logo.png"></img>
      <h3>Por favor haga click en Login</h3>
      <Button
        variant="contained"
        className="button-login"
        onClick={handleClick}
      >
        Login
      </Button>
    </div>
  );
};
