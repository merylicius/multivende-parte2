import React, { useEffect } from 'react'
import { Redirect, useLocation  } from 'react-router'
import { consult, islogged } from '../../services/Service';


export const Configuration = () => {

    const { search } = useLocation();
    const query = new URLSearchParams(search);
    const codeparam = query.get('code')

    useEffect(() => {
        consult(codeparam)
    }, [codeparam])
    const logged = islogged();
    console.log('logged', logged)

    return (
        <div>

            {logged ? <Redirect to="/home"  /> :
                    <Redirect to="/login"  />
            }
            
        </div>
    )
}