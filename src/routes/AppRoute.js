import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Login } from "../components/auth/Login";
import { DashboardRoutes } from "./DashboardRoutes";
import PrivateRoute from "./PrivateRoute";

export const AppRouter = () => {
  return (
    <>
      <Router>
        <div>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route path="/" component={DashboardRoutes} />
            {/* <PrivateRoute exact path="/" component={DashboardRoutes}/> */}
          </Switch>
        </div>
      </Router>
    </>
  );
};
