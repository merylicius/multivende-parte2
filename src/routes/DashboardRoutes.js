import React from 'react';
import {
  
    Switch,
    Route,
    Redirect,

} from "react-router-dom";

import { Configuration } from '../components/auth/Configuration';
import { Stock } from '../components/pages/Stock';

import { CreateProvider } from '../components/pages/CreateProvider';
import { EditProvider } from '../components/pages/EditProvider';
import { Home } from '../components/pages/Home';
import { NavBar } from '../components/pages/NavBar';
import { Provider } from '../components/pages/Provider';
import PrivateRoute from './PrivateRoute';


export const DashboardRoutes = () => {


    return (
        <>
        

                <div className=" mt-2">
                <NavBar/>

                    <Switch>

                        <Route exact path="/" component={Home} />
                        <Route exact path="/configuration" component={Configuration} />
                        <Route exact path="/provider" component={Provider} />
                        <Route exact path="/createprovider" component={CreateProvider} />
                        <Route exact path="/editarprovider/:providerId" component={EditProvider} />
                        <Route exact path="/stock" component={Stock} />
 

                        <Redirect to="/" />
                    </Switch>
                </div>

            
        </>
    )
}